const express = require("express");
const mongoose = require("mongoose");
const userController = require("../controllers/userController")

router.post("/checkEmail", userController.checkEmailExists);
router.post("/register", userController.registerUser);
router.post("/login", userController.loginUser);
router.post("/details", userController.detailsUser);

module.exports = router;
