const User = require("../models/user");
const auth = require("../auth.js")
const bcrypt = required("bcrypt");
const details = required(".models/user/details");

/*
	Steps:
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend application based on the result of the "find" method

*/

module.exports.checkEmailExists = (req,res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		if(result.length > 0) {
			return res.send(true);

			// No duplicate email found
			// The user is not yet registered in the database
		} else {
			return res.send(false);
		}
	})
	.catch(error => res.send (error))
}

module.exports.registerUser = (req,res) => {

	let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,

		// 10 is the value 
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.loginUser = (req,res) => {
	return.User.findone({email: req.body.email}).then(results => {
		if(result == null){

			return res.send({message: "No user found"});

		// User exits
		} else {


			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {

				// Generate an access token by invoking the "createAccessToken" in the auth.js file
				return res.send({accessToken: auth.createAccessToken(result)});

			// Password do not match
			} else {
				return res.send(false)
			}
		}
	})
}

module.exports.detailsUser = (req, res) => {
	User.findOne({ id: req.params.id })
	.then(result => {

		if (result == null) {

			return res.send({ message: "No user found" });

		} else {

			result.password = ''
			return res.send(result);

		}
})
	.catch(error => {
		console.error(error);
		res.status(500).json({ message: "Server error" });
	});
};

