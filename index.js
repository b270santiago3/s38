const express = require("express");
const mongoose = require("mongoose");
const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin123@zuiit.mgyqbdp.mongodb.net/b270_booking?retryWrites=true&w=majority",

	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

// Set notifications for database connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Will use the defined port number for the application whenever an environment variable is available OR will use port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});